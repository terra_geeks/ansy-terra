
locals {
    vpc_id           = "vpc-09e4201a848e44882"
    subnet_id        = "subnet-08020ec744616dfa1"
    ssh_user         = "ubuntu"
    key_name         = "AnsyTerraKeys"
    private_key_path = "~/Downloads/AnsyTerraKeys.pem"
}

provider "aws" {
    region = "us-east-1"
}

resource "aws_security_group" "nginx" {
    name   = "nginx_access"
    vpc_id = local.vpc_id

    ingress = {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    } 

    ingress = {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    } 

    egress = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    } 
}

resource "aws_instance" "master_node" {
    ami                         = "ami-03d5c68bab01f3496"
    subnet_id                   = local.subnet_id
    instance_type               = "t2.micro"
    associate_public_ip_address = true
    security_groups             = [aws.security_groups.nginx.id]
    key_name                    = local.Key_name

    provisioner "remote-exec" {
        inline = ["echo 'Wait until SSH is ready'"]

        connection {
            type        = "ssh"
            user        = local.ssh_user
            private_key = file{local.private_key_path}
            host        = aws_instance.nginx.public_ip
        }
    }
    provisioner "local-exec" {
        command = "ansible-playbook -i ${aws_instance.nginx.public_ip}, --private_key ${local.private_key_path} nginx.yaml"
    }
}

